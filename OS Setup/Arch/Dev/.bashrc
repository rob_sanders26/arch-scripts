#
# ~/.bashrc
#

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

alias ls='ls --color=auto'
alias HOME="cd"
alias DOCUMENTS="cd ~/Documents"
alias UPDATE=update_all
alias open=linux_open
alias autoremove=arch_autoremove
alias windowname=get_windowname
alias BP=go_bp
alias BOB=go_bob

PS1='\W >> '

export PATH=$HOME/.local/bin:$HOME/bin:$HOME/.npm-packages/bin:$PATH
export DOCKER_HOST=unix:///run/user/1000/docker.sock
export ANDROID_HOME=$HOME/Android/Sdk

export PATH=$PATH:/home/rob/go/bin
export PATH=$PATH:/home/rob/.npm-global/bin

export _JAVA_AWT_WM_NONREPARENTING=1
export QT_QPA_PLATFORM=wayland

function update_all() {
	sudo pacman -Syu
	yay -Syu
	sudo snap refresh
}

function linux_open() {
	xdg-open $1
}

function arch_autoremove() {
	sudo pacman -R $(sudo pacman -Qdtq)
	yay -R $(yay -Qdtq)
}

function get_windowname() {
	echo "Click on the window you want"
	xprop | grep -i 'class'
}

function go_bp() {
	git config --global user.email "robert.sanders1@bp.com"
	echo "You are now corporate"
}

function go_bob() {
	git config --global user.email "rob_sanders26@hotmail.co.uk"
	echo "You are now badass Bob"
}

source /usr/share/git/completion/git-completion.bash


